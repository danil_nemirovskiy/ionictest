import {NgModule} from '@angular/core';
import {CompaniesStorageService} from './services/storage/companies-storage.service';
import {CommonModule} from '@angular/common';
import {HttpClientModule} from '@angular/common/http';
import {ScrollHideDirective} from './directories/hide-header';
import {GooglePhotosStorageService} from './services/storage/google-photos-storage.service';
import {BasicComponent} from './components/basic/basic.component';
import {GooglePictureComponent} from './components/google-picture/google-picture.component';
import {IonicModule} from '@ionic/angular';
import {GoogleLocationComponent} from './components/google-location/google-location.component';
import {GoogleLocationStorageService} from './services/storage/google-location-storage.service';


@NgModule({
    declarations: [
        BasicComponent,
        ScrollHideDirective,
        GooglePictureComponent,
        GoogleLocationComponent
    ],
    exports: [
        BasicComponent,
        ScrollHideDirective,
        GooglePictureComponent,
        GoogleLocationComponent
    ],
    entryComponents: [],
    imports: [CommonModule, HttpClientModule, IonicModule],
    providers: [CompaniesStorageService, GooglePhotosStorageService, GoogleLocationStorageService],
})
export class SharedModule {
}
