import {Component, Input, OnInit} from '@angular/core';
import {GooglePhotosParameters} from '../../models/google-photos-parameters';
import {GooglePhotosStorageService} from '../../services/storage/google-photos-storage.service';
import {BasicComponent} from '../basic/basic.component';

@Component({
    selector: 'app-google-picture',
    templateUrl: './google-picture.component.html',
    styleUrls: ['./google-picture.component.scss'],
})
export class GooglePictureComponent extends BasicComponent implements OnInit {

    @Input() latitude: number;
    @Input() longitude: number;
    @Input() elementSize: string;

    googlePhotoImage: string;

    constructor(private googlePhotosStorageService: GooglePhotosStorageService) {
        super();
    }

    ngOnInit() {
        this.onLoadGooglePhotosByCoordinates();
    }

    onLoadGooglePhotosByCoordinates() {

        const parameters = new GooglePhotosParameters(
            this.latitude + ' + ' + this.longitude,
            '16',
            '1',
            this.elementSize,
            'satellite',
            'png',
            true,
        );


        this.subs.push(
            this.googlePhotosStorageService.getPhotoByCoordinates(parameters)
                .subscribe(
                    (response) => {
                    },
                    (error) => {
                        this.googlePhotoImage = error.url;
                    }
                )
        );

    }

}
