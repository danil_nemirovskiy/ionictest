import {Component, Input, OnInit} from '@angular/core';
import {GoogleLocationStorageService} from '../../services/storage/google-location-storage.service';
import {GoogleLocationParameters} from '../../models/google-location-parameters';
import {BasicComponent} from '../basic/basic.component';

@Component({
    selector: 'app-google-location',
    templateUrl: './google-location.component.html',
    styleUrls: ['./google-location.component.scss'],
})
export class GoogleLocationComponent extends BasicComponent implements OnInit {

    @Input() latitude: number;
    @Input() longitude: number;
    @Input() elementSize: string;


    googleMapWithMarker: string;

    constructor(private googleLocationStorageService: GoogleLocationStorageService) {
        super();
    }

    ngOnInit() {
        this.loadLocationWithMarker();
    }


    loadLocationWithMarker() {

        const parameters = new GoogleLocationParameters(
            this.latitude + ',' + this.longitude,
            '16',
            this.elementSize,
            this.latitude + ',' + this.longitude
        );

        this.subs.push(
            this.googleLocationStorageService.getMapWithMarker(parameters)
                .subscribe(
                    (response) => {
                        console.log(response);
                    },
                    (error) => {
                        this.googleMapWithMarker = error.url;
                    }
                )
        );

    }

}
