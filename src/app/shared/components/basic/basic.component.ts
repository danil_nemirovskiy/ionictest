import {Component, OnDestroy} from '@angular/core';
import {Subscription} from 'rxjs';

@Component({
    selector: 'app-abstract',
    templateUrl: './basic.component.html',
    styleUrls: ['./basic.component.scss'],
})
export class BasicComponent implements OnDestroy {

    protected subs: Subscription[] = [];

    ngOnDestroy(): void {
        this.subs.forEach((sub) => sub && sub.unsubscribe());
    }
}