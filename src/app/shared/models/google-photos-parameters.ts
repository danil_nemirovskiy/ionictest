import {HttpParams} from '@angular/common/http';
import {keys} from '../const';


export class GooglePhotosParameters {
    public center: string;
    public zoom: string;
    public scale: string;
    public size: string;
    public maptype: string;
    public format: string;
    public visual_refresh: boolean;

    constructor(
        center: string,
        zoom: string,
        scale: string,
        size: string,
        maptype: string,
        format: string,
        visual_refresh: boolean,
    ) {
        this.center = center;
        this.zoom = zoom;
        this.scale = scale;
        this.size = size;
        this.maptype = maptype;
        this.format = format;
        this.visual_refresh = visual_refresh;
    }

    getGooglePhotosParams(): HttpParams {
        let params: HttpParams = new HttpParams();
        params = params.append('center', this.center);
        params = params.append('zoom', this.zoom);
        params = params.append('scale', this.scale);
        params = params.append('size', this.size);
        params = params.append('maptype', this.maptype);
        params = params.append('format', this.format);
        params = params.append('visual_refresh', this.visual_refresh.toString());
        params = params.append('key', keys.GOOGLE_MAP_API_KEY);

        return params;
    }
}