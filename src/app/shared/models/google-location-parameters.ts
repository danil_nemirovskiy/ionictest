import {HttpParams} from '@angular/common/http';
import {keys} from '../const';


export class GoogleLocationParameters {
    public center: string;
    public zoom: string;
    public size: string;
    public markers: string;

    constructor(
        center: string,
        zoom: string,
        size: string,
        markers: string
    ) {
        this.center = center;
        this.zoom = zoom;
        this.size = size;
        this.markers = markers;
    }

    getGoogleLocationParams(): HttpParams {

        let params: HttpParams = new HttpParams();

        params = params.append('center', this.center);
        params = params.append('zoom', this.zoom);
        params = params.append('size', this.size);
        params = params.append('markers', this.markers);
        params = params.append('key', keys.GOOGLE_MAP_API_KEY);


        return params;
    }

}