import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import {Company} from '../models/company';

@Injectable({
    providedIn: 'root'
})
export class CompaniesService {

    get companies() {
        return this.companiesChanged$.getValue();
    }

    companiesChanged$ = new BehaviorSubject([]);

    constructor() {
    }


    setCompanies(companies: Company[]) {
        this.companiesChanged$.next(companies);
    }

    onDeleteCompany(companyId) {
        const newArray = this.companies.filter((company: Company) => {
            return company.id !== companyId;
        });
        this.companiesChanged$.next(newArray);
    }

    onDeleteAllCompanies() {
        this.companiesChanged$.next([]);
    }

    getCompanyById(companyId: number) {
        return this.companies.find((company: Company) => company.id === companyId);
    }
}
