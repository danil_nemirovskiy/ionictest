import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {api} from '../../const';
import {GooglePhotosParameters} from '../../models/google-photos-parameters';

@Injectable()

export class GooglePhotosStorageService {

    constructor(private http: HttpClient) {
    }

    getPhotoByCoordinates(parameters: GooglePhotosParameters) {

        const url = api.googleMapsApi;

        const params = parameters.getGooglePhotosParams();

        return this.http.get(url, {params: params});
    }
}