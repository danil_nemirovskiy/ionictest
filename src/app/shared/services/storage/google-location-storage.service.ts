import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {api} from '../../const';
import {GoogleLocationParameters} from '../../models/google-location-parameters';

@Injectable()

export class GoogleLocationStorageService {

    constructor(private http: HttpClient) {
    }

    getMapWithMarker(parameters: GoogleLocationParameters) {

        const url = api.googleMapsApi;

        const params = parameters.getGoogleLocationParams();

        return this.http.get(url, {params: params});
    }
}
