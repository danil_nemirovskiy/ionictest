import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable()

export class CompaniesStorageService {

    constructor(private http: HttpClient) {
    }

    getCompanies(): Observable<Object> {
        return this.http.get('../../assets/sources/companies.json');
    }

}