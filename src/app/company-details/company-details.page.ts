import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {CompaniesService} from '../shared/services/companies.service';
import {Company} from '../shared/models/company';
import {BasicComponent} from '../shared/components/basic/basic.component';

@Component({
    selector: 'app-company-details',
    templateUrl: './company-details.page.html',
    styleUrls: ['./company-details.page.scss'],
})
export class CompanyDetailsPage extends BasicComponent implements OnInit {

    loadedCompany: Company;
    companyDataSchema: {}[];

    translate = {
        pageTitle: {
            en: 'Company Details',
            iv: 'פרטי החברה'
        }
    };

    googlePictureSize = '200x200';
    googleMapSize = '300x300';

    constructor(private companiesService: CompaniesService,
                private activatedRoute: ActivatedRoute) {
        super();
    }

    ngOnInit() {
        this.getLoadedCompany();

        this.initCompanyDataSchema();
    }


    getLoadedCompany() {
        this.subs.push(
            this.activatedRoute.paramMap.subscribe(
                (paramMap) => {
                    if (!paramMap.has('companyId')) {
                        return;
                    }

                    const companyId = Number(paramMap.get('companyId'));

                    this.loadedCompany = this.companiesService.getCompanyById(companyId);
                }
            )
        );
    }


    initCompanyDataSchema() {
        this.companyDataSchema = [
            {
                property: this.loadedCompany.name,
                className: 'ion-text-center ion-align-self-center'
            },
            {
                property: this.loadedCompany.address,
                className: 'ion-text-center ion-align-self-center'
            },
        ]
    }
}
