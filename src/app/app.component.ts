import {Component} from '@angular/core';

import {Platform} from '@ionic/angular';
import {SplashScreen} from '@ionic-native/splash-screen/ngx';
import {StatusBar} from '@ionic-native/status-bar/ngx';
import {filter} from 'rxjs/operators';
import {NavigationStart, Router} from '@angular/router';
import {BasicComponent} from './shared/components/basic/basic.component';

@Component({
    selector: 'app-root',
    templateUrl: 'app.component.html'
})
export class AppComponent extends BasicComponent {

    directionOfText: string;

    constructor(
        private platform: Platform,
        private splashScreen: SplashScreen,
        private statusBar: StatusBar,
        private router: Router
    ) {
        super();
        this.initializeApp();
    }

    initializeApp() {
        this.platform.ready().then(() => {
            this.statusBar.styleDefault();
            this.splashScreen.hide();
        });

        this.onRefreshApp();
    }


    onRefreshApp() {
        this.subs.push(
            this.router.events
                .pipe(filter((e) => e instanceof NavigationStart && e.id === 1))
                .subscribe(({url}: NavigationStart) => {
                    if (url && url !== '/home') {
                        this.router.navigate(['/home']);
                    }
                })
        );
    }
}
