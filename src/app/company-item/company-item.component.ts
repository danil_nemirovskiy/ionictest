import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Company} from '../shared/models/company';
import {GooglePhotosStorageService} from '../shared/services/storage/google-photos-storage.service';
import {GooglePhotosParameters} from '../shared/models/google-photos-parameters';
import {CompaniesService} from '../shared/services/companies.service';
import {Router} from '@angular/router';

@Component({
    selector: 'app-company-item',
    templateUrl: './company-item.component.html',
    styleUrls: ['./company-item.component.scss'],
})
export class CompanyItemComponent implements OnInit {

    @Input() company: Company;

    googlePictureSize = '100x100';

    constructor(private companiesService: CompaniesService) {
    }

    ngOnInit() {
    }


    onDeleteCompany(companyId) {
        this.companiesService.onDeleteCompany(companyId);

        // for not navigate to the company details page

        event.stopPropagation();
    }

}
