import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {IonicModule} from '@ionic/angular';
import {FormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';

import {AgmCoreModule} from '@agm/core';

import {HomePage} from './home.page';
import {SharedModule} from '../shared/shared.module';
import {CompanyItemComponent} from '../company-item/company-item.component';
import {keys} from '../shared/const';


@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        SharedModule,
        AgmCoreModule.forRoot({
            apiKey: keys.GOOGLE_MAP_API_KEY,
            libraries: ['places'],
            language: 'en',
        }),
        RouterModule.forChild([
            {
                path: '',
                component: HomePage
            }
        ]),
    ],
    declarations: [HomePage, CompanyItemComponent]
})
export class HomePageModule {
}
