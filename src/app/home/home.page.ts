import {Component, OnInit, ViewChild} from '@angular/core';
import {ScrollHideConfig} from '../shared/directories/hide-header';
import {CompaniesStorageService} from '../shared/services/storage/companies-storage.service';
import {Company} from '../shared/models/company';
import {BasicComponent} from '../shared/components/basic/basic.component';
import {CompaniesService} from '../shared/services/companies.service';
import {IonInfiniteScroll, IonVirtualScroll} from '@ionic/angular';

declare var cordova;

@Component({
    selector: 'app-home',
    templateUrl: 'home.page.html',
    styleUrls: ['home.page.scss'],
})
export class HomePage extends BasicComponent implements OnInit {

    @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;
    @ViewChild(IonVirtualScroll) virtualScroll: IonVirtualScroll;

    headerScrollConfig: ScrollHideConfig = {cssProperty: 'margin-top', maxValue: 54};

    companies: Company[];

    fabButtonsSchema: {}[];

    constructor(private companiesStorageService: CompaniesStorageService,
                private companiesService: CompaniesService) {
        super();
    }

    ngOnInit(): void {


        this.initFabButtonsSchema();

        this.subs.push(
            this.companiesStorageService.getCompanies().subscribe(
                (response) => {
                    this.companies = response['companies'];

                    // assign id for remove company by unique property

                    this.companies.forEach((company: Company, index) => {
                        company.id = index;
                    });
                    this.companiesService.setCompanies(this.companies);
                }
            ),

            this.companiesService.companiesChanged$.subscribe(
                (companies: Company[]) => {
                    this.companies = companies;
                }
            )
        );
    }


    initFabButtonsSchema() {
        this.fabButtonsSchema = [
            {
                side: 'top',
                buttonColor: 'medium',
                iconName: 'add',
                clickEvent: this.onAddNewCompany.bind(this)
            },
            {
                side: 'start',
                buttonColor: 'danger',
                iconName: 'trash',
                clickEvent: this.onDeleteAllCompanies.bind(this)
            },
            {
                side: 'end',
                buttonColor: 'warning',
                iconName: 'exit',
                clickEvent: this.onCloseApp.bind(this)
            },
        ];
    }

    onCloseApp() {
        cordova.plugins.exit();
    }


    onDeleteAllCompanies() {
        this.companiesService.onDeleteAllCompanies();
    }

    onAddNewCompany() {

    }

}
